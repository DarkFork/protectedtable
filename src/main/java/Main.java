public class Main {

    public static void Output(int lines, int columns, int a, int b, int[][] table ) {

        for(int i = 0; i < lines; i++) {
            for(int j = 0; j < columns; j++) {

                if (i == a && j ==b) {      //exclude cell (X = 2; Y =3)
                    continue;               //iteration finish
                }

                System.out.print( table[i][j] + " ");

            }

            System.out.println();

            if (i > 10000) {
                System.out.print("ENDLESS CYCLE!");
                break;                      //cycle finish
            }
        }

    }

    public static void main(String[] args) {

        int X = 3;              //lines
        int Y = 3;              //columns

        int[][] mas = new int[][]
                {
                        {1, 1, 1},
                        {1, 1, 1},
                        {1, 1, 1}
                };

        Output(X, Y, 1, 2, mas);

    }

}
